There is a problem adding users to mailproxy.

To add a user to "alice" to mailproxy in our config, you execute:

/go/bin/mailproxy -r \     # command to generate config for a user
-authority 172.28.1.2:29483 \    # the authority
-authorityKey "o4w1Nyj/nKNwho5SWfAIfh7SMU8FRx52nMHGgYsMHqQ=" \    #public key of authority
-registrationAddr 172.28.1.3:36968 \  # Addr+Port specified by UserRegistrationHTTPAddresses in Providers katzenpost.toml file
-registrationWithoutHttps \	#yeah
-provider provider1 \	 #name of provider
-providerKey "2krwfNDfbakZCSTUUZYKXwdduzlEgS9Jfwm7eyZ0sCg=" \    #Public key of provider
-account alice    #name of account

By default it generates a directory ~/.mailproxy for the mailproxy.toml file and for the user keys.
You can change this default directory using the -dataDir option.

So far, so good. User alice is created both in the mailproxy config, and registered in the users.db file at the provider (which is a BoltDB file).

Trouble starts when you try to register a second user, lets say "bob", registered at provider 2.

I used following command in our configuration:

/go/bin/mailproxy -r \
-authority 172.28.1.2:29483 \
-authorityKey "o4w1Nyj/nKNwho5SWfAIfh7SMU8FRx52nMHGgYsMHqQ=" \
-registrationAddr 172.28.1.4:36967 \
-registrationWithoutHttps \
-provider provider2 \
-providerKey "imigzI26tTRXyYLXujLEPI9QrNYOEgC4DElsFdP9acQ=" \
-account bob

Then mailproxy doesnt check if a configuration in ~/mailproxy already exists, but says something like "Folder exists. Error. Exiting."
In other words, i found no way to add a user to an existing configuration. Mailproxy always wants to generate a new configuration.

What I did to make it working was the following:

I executed both commands with a different -dataDir folder.

then I created a third folder for the real mailproxy configuration.
I copied the folders "alice@provider1" and "bob@provider2" in that folder and, finally, merged the now two existing mailproxy.toml files into one manually using a text editor.


Proposed solution:

Mailproxy should have a command to ADD a user.

And:

Abandon the bolt DB file format in provider config. 
Make a .txt or a .toml file for the users, please.

